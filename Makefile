tag = `git describe --abbrev=4 --always --tags`

all: build test
	afplay -t 0.3 ~/Active/norske-hav-epub/originals/signal\ ok.aif 

build:
	swiftformat Sources/.
	swift build
	echo $(tag)

test:
	swift test

publish: build test	
	git add .
	git commit -m "autocommit $(tag)"
	git push
	git log --oneline

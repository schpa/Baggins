import Foundation

public func first<A, B, C, D, AA>(_ f: @escaping (A) -> AA) -> ((A, B, C, D)) -> (AA, B, C, D) {
    return { pair in
        (pair.0 |> f, pair.1, pair.2, pair.3)
    }
}

public func second<A, B, C, D, BB>(_ f: @escaping (B) -> BB) -> ((A, B, C, D)) -> (A, BB, C, D) {
    return { pair in
        (pair.0, f(pair.1), pair.2, pair.3)
    }
}

public func third<A, B, C, D, CC>(_ f: @escaping (C) -> CC) -> ((A, B, C, D)) -> (A, B, CC, D) {
    return { pair in
        (pair.0, pair.1, f(pair.2), pair.3)
    }
}

public func fourth<A, B, C, D, DD>(_ f: @escaping (D) -> DD) -> ((A, B, C, D)) -> (A, B, C, DD) {
    return { pair in
        (pair.0, pair.1, pair.2, f(pair.3))
    }
}
